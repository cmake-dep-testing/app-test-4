
#include <iostream>
#include <z/z.h>
#include "y/y.h"

namespace y {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingY(uint indentation) {
    addIndentation(indentation);
    std::cout << "Y v1" << std::endl;

    z::doThingZ(indentation + 1);
  };
}
