cmake_minimum_required(VERSION 3.13)

if (NOT TARGET y::y)
  add_subdirectory(extern/libz)
  add_library(y STATIC src/y.cpp)
  target_link_libraries(y PRIVATE z)
  target_include_directories(y PUBLIC include)
  add_library(y::y ALIAS y)
endif()
