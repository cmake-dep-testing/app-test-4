
#include <iostream>
#include "z/z.h"

namespace z {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingZ(uint indentation) {
    addIndentation(indentation);
    std::cout << "Z v1" << std::endl;
  };
}
