
#include <iostream>
#include "x/x.h"
#include <z/z.h>

namespace x {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingX(uint indentation) {
    addIndentation(indentation);
    std::cout << "X v1" << std::endl;

    z::doThingZ(indentation + 1);
  };
}
