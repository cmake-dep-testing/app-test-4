
#include <iostream>
#include <x/x.h>
#include <y/y.h>

int main() {
  std::cout << "main" << std::endl;
  x::doThingX();
  y::doThingY();

  return 0;
}
